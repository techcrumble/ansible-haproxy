#!/bin/bash

REF="main"
ROOT=/
MODULE_DIR="ansible-haproxy"
DEPLOY_USER="haproxy"
DEPLOY_TOKEN="rDirxrYH1LNJQtTMH8tp"

run_ansible() {
  ansible-galaxy remove haproxy
  ansible-galaxy install -r requirements.yml
  ansible-playbook haproxy_playbook.yaml --extra-vars "@minions.yaml"
}

cd "$ROOT"
if [[ -d "$MODULE_DIR" ]]; then
        rm -rf "$MODULE_DIR"
else
        git clone https://"$DEPLOY_USER":"$DEPLOY_TOKEN"@gitlab.com/techcrumble/ansible-haproxy.git  --branch "$REF" --single-branch
        cd "$MODULE_DIR"
        run_ansible
fi
